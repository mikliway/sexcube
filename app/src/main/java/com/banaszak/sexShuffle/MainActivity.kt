package com.banaszak.sexShuffle

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener {
            rollSex()
        }

    }

    private fun rollSex() {
        val sexPosition = AmountOfSex(38)
        val sexRoll = sexPosition.roll()

        val sexPositionImage: ImageView = findViewById(R.id.imageView)


        when (sexRoll) {
            1 -> sexPositionImage.setImageResource(R.drawable.number1)
            2 -> sexPositionImage.setImageResource(R.drawable.number2)
            3 -> sexPositionImage.setImageResource(R.drawable.number3)
            4 -> sexPositionImage.setImageResource(R.drawable.number4)
            5 -> sexPositionImage.setImageResource(R.drawable.number5)
            6 -> sexPositionImage.setImageResource(R.drawable.number6)
            7 -> sexPositionImage.setImageResource(R.drawable.number7)
            8 -> sexPositionImage.setImageResource(R.drawable.number8)
            9 -> sexPositionImage.setImageResource(R.drawable.number9)
            10 -> sexPositionImage.setImageResource(R.drawable.number10)
            11 -> sexPositionImage.setImageResource(R.drawable.number11)
            12 -> sexPositionImage.setImageResource(R.drawable.number12)
            13 -> sexPositionImage.setImageResource(R.drawable.number13)
            14 -> sexPositionImage.setImageResource(R.drawable.number14)
            15 -> sexPositionImage.setImageResource(R.drawable.number15)
            16 -> sexPositionImage.setImageResource(R.drawable.number16)
            17 -> sexPositionImage.setImageResource(R.drawable.number17)
            18 -> sexPositionImage.setImageResource(R.drawable.number18)
            19 -> sexPositionImage.setImageResource(R.drawable.number19)
            20 -> sexPositionImage.setImageResource(R.drawable.number20)
            21 -> sexPositionImage.setImageResource(R.drawable.number21)
            22 -> sexPositionImage.setImageResource(R.drawable.number22)
            23 -> sexPositionImage.setImageResource(R.drawable.number23)
            24 -> sexPositionImage.setImageResource(R.drawable.number24)
            25 -> sexPositionImage.setImageResource(R.drawable.number25)
            26 -> sexPositionImage.setImageResource(R.drawable.number26)
            27 -> sexPositionImage.setImageResource(R.drawable.number27)
            28 -> sexPositionImage.setImageResource(R.drawable.number28)
            29 -> sexPositionImage.setImageResource(R.drawable.number29)
            30 -> sexPositionImage.setImageResource(R.drawable.number30)
            31 -> sexPositionImage.setImageResource(R.drawable.number31)
            32 -> sexPositionImage.setImageResource(R.drawable.number32)
            33 -> sexPositionImage.setImageResource(R.drawable.number33)
            34 -> sexPositionImage.setImageResource(R.drawable.number34)
            35 -> sexPositionImage.setImageResource(R.drawable.number35)
            36 -> sexPositionImage.setImageResource(R.drawable.number36)
            37 -> sexPositionImage.setImageResource(R.drawable.number37)
            38 -> sexPositionImage.setImageResource(R.drawable.number38)
        }
    }
}

class AmountOfSex(val number: Int){
    fun roll(): Int {
        return (1..number).random()
    }
}